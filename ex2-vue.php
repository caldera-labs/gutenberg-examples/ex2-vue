<?php
/**
 * Plugin Name:     Learn Gutenberg Example 2: VueJS
 * Plugin URI:      https://learn.wordpress.org
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     vuetest
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Vuetest
 */
include_once  __DIR__ . '/blocks/block.php';
